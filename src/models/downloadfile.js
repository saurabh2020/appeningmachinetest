let mongoose = require('mongoose');

let schema = mongoose.Schema;

let dowmloadSchema = new schema({
    fileName: String,
    fileId: { type: mongoose.Types.ObjectId, ref: 'encryptfile' },
    userId: { type: mongoose.Types.ObjectId, ref: 'user' },
    ip: String,
    location: {
        type: { type: String, default: "Point", enum: ["Point"] },
        coordinates: { type: [Number], }
    },
    createAt: Number,
    updateAt: Number,


}, { timestamps: true })

dowmloadSchema.index({ location: "2dsphere" });


const downloadModel = mongoose.model(
    "filedownload", dowmloadSchema
);

module.exports = { downloadModel };