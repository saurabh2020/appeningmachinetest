let mongoose = require('mongoose');

let schema = mongoose.Schema;

let userSchema = new schema({
    type: {
        type: String, enum: ['admin', 'user'], default: 'user'
    },
    emailId: { type: String, lowercase: true },
    password: String,
    createAt: Number,
    updateAt: Number,



}, { timestamps: true })


const userModel = mongoose.model(
    "user", userSchema
);

module.exports = { userModel };