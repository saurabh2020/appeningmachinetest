const mongoUrls = {
    local: "mongodb://localhost:27017/appeningtest"

};

const config = {
    userSecret: 'thisisuser',
    adminSecret: 'thisisadmin',
    tokenValidity: {
        days: '1 days'
    },
    encryptionKey: 'thisissecretkey',
    algo: 'aes-256-ctr',
    hashalgo: 'sha256'

};



const setUp = function (env) {
    return mongoUrls[env];
};

module.exports = { config, setUp, };
