let bcrypt = require('bcrypt');
let saltRounds = 10;

// const hash = (password, callback) => {
//     bcrypt.genSalt(saltRounds, function (err, salt) {
//         bcrypt.hash(password, salt, function (err, hash) {
//             callback(err, hash);
//         });
//     });
// };


const hash = async (password) => {
    let salt = await bcrypt.genSalt(saltRounds);
    let hash = await bcrypt.hash(password, salt);
    return hash;
}

const checkHashPassword = function (password, hash, callback) {
    bcrypt.compare(password, hash, function (err, res) {
        callback(err, res);
    });
};

module.exports = { hash, checkHashPassword }

