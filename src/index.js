const http = require("http");
const app = require("./app");
const port = 8028;
const server = http.createServer(app);
const { dbConnect } = require("../src/mongooseDB/connectiondb");
const { createAdmin, createuser } = require("../src/controller/userCreate");


createAdmin('hrhk@1234')
createuser('123456');

dbConnect("local").then(_ => {
    server.listen(port, _ => console.log(`Server is Running on port ${port}`));
});

module.exports.port = port;
