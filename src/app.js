/**
 * Module dependencies.
 */

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader(
        "Access-Control-Allow-Methods",
        "GET, POST, PUT, PATCH, DELETE,OPTIONS"
    );
    res.setHeader(
        "Access-Control-Allow-Headers",
        "X-Requested-With,content-typeAccept, Authorization,authKey"
    );
    res.setHeader("Access-Control-Allow-Credentials", true);
    next();
});

let route = require('./routes');
app.use('/api', route.apiRoutes);

module.exports = app;