let express = require('express');
let controller = require('../controller');
let apiRoutes = express.Router();

apiRoutes.get('/home', controller.home);


apiRoutes.post('/login', controller.login);


apiRoutes.use(controller.middleware);
apiRoutes.post('/generate/encrpt/file', controller.encrpt);
apiRoutes.post('/decrpt/file', controller.decrpt);
apiRoutes.get('/download', controller.download);
apiRoutes.get('/download/history/list', controller.getDownloadlist); //u can use query parameter for sorting and filter





module.exports = apiRoutes;

