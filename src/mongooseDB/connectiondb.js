const mongoose = require("mongoose");
const { setUp } = require("../config/env");


const dbConnect = async function (env) {
    mongoose.set("useCreateIndex", true);
    mongoose.set('useFindAndModify', false);
    const mongoUrl = setUp(env);
    try {
        console.log("Establishing Mongo DB Connection...");
        const x = await mongoose.connect(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true });
        console.log(`Mongo DB Connected ${mongoUrl} :)`);
        return false;
    } catch (error) {
        console.log("==== DB Connection Error ====", error.message);
        throw error;
    }
};

module.exports = { dbConnect };