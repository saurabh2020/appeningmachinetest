const crypto = require('crypto');
var fs = require('fs');
let { config } = require('../config/env')

const algorithm = config.algo;
let key = config.encryptionKey;
key = crypto.createHash(config.hashalgo).update(String(key)).digest();

let { encryptFileNodel } = require('../models/encrptfile')


const encrypt = (req, res, next) => {
    try {
        let buffer = new Buffer(req.body.data ? req.body.data : 'this is the text data'); //  You can pass message/data by api url
        const iv = crypto.randomBytes(16);
        const cipher = crypto.createCipheriv(algorithm, key, iv);
        const result = Buffer.concat([iv, cipher.update(buffer), cipher.final()]);
        const fileName = new Date().getTime() + '.txt'
        fs.open(`${__dirname}/files/${fileName}`, 'w', function (err, fd) {
            if (err) {
                return res.status(400).json({ message: 'could not open file: ', err, error: true })
            }
            fs.write(fd, result, 0, result.length, null, function (err) {
                if (err) {
                    return res.status(400).json({ message: 'error writing file', err, error: true })

                }
                fs.close(fd, function () {
                    console.log('wrote the file successfully');
                    req.data = {
                        fileName
                    }
                    next()
                });
            });
        });
    }
    catch (err) {
        return res.status(500).json({ message: 'Internal server Error' + err })
    }
};


let createFilePath = (req, res) => {
    try {
        encryptFileNodel.create({
            fileName: req.data.fileName,
            path: `${__dirname}/files/${req.data.fileName}`,
        }, (err, created) => {
            if (err) {
                return res.status(400).json({ success: false, message: 'error occured in createFilePath ', err })
            }
            else {
                return res.status(400).json({ success: true, message: 'file generate successfully', })
            }

        })
    }
    catch (err) {
        return res.status(500).json({ message: 'Internal server Error' + err })
    }
}

module.exports = [
    encrypt,
    createFilePath

]