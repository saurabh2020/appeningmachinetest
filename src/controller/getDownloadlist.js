let { downloadModel } = require('../models/downloadfile')

module.exports = (req, res) => {
    try {

        let condition = [];
        if (req.query.asc && !req.query.dsc) {
            condition.push({
                $sort: {
                    createAt: 1
                }
            })
        }
        else if (req.query.dsc && !req.query.asc) {
            condition.push({
                $sort: {
                    createAt: -1
                }
            })
        }
        else if (req.query.isLocation) {
            condition.push({
                $geoNear: {
                    near: { type: "Point", coordinates: [Number(req.body.lng), Number(req.body.lat)] },
                    spherical: true,
                    //query: { },
                    distanceField: "calcDistance"
                }
            })
        }

        else {
            condition = [
                {
                    $sort: {
                        createAt: -1
                    }
                },
                {
                    $lookup: {
                        from: "users",
                        localField: "userId",
                        foreignField: "_id",
                        as: "user"
                    }
                },
                {
                    $unwind: {
                        path: "$user",
                        preserveNullAndEmptyArrays: false
                    }
                },
            ]
        }
        downloadModel.aggregate(condition, (err, downloadList) => {
            if (err) {
                return res.status(400).json({ success: false, message: 'something went wrong', err })
            }
            else if (!downloadList || downloadList.length == 0) {
                return res.status(201).json({ success: false, message: 'no download list found' })
            }
            else {
                return res.status(200).json({ success: true, message: `${downloadList.length} records found`, downloadList: downloadList })
            }
        })
    }
    catch (err) {
        return res.status(500).json({ message: 'Internal server Error' + err })
    }
}