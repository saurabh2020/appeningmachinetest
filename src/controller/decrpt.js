const crypto = require('crypto');
var fs = require('fs');

let { config } = require('../config/env')

const algorithm = config.algo;
let key = config.encryptionKey;
key = crypto.createHash(config.hashalgo).update(String(key)).digest();

let readFile = (req, res, next) => {
    fs.readFile(`${__dirname}/files/${req.body.fileName}`, (err, data) => {
        if (err) {
            return res.status(200).json({ error: true, message: 'no such a file ', err })
        }
        const decrypted = decrypt(data);//
        console.log('Decrypted:', decrypted.toString());
        fs.writeFile(`${__dirname}/files/decypt${req.body.fileName} `, decrypted.toString(), (err, writeFile) => {
            if (err) {
                return res.status(400).json({ error: true, message: 'cam not write file ', err })
            }
            else {
                return res.status(400).json({ error: true, message: 'can not write file ', err })
            }
        })

    })
}


const decrypt = (encrypted) => {
    // Get the iv: the first 16 bytes
    const iv = encrypted.slice(0, 16);
    // Get the rest
    encrypted = encrypted.slice(16);
    // Create a decipher
    const decipher = crypto.createDecipheriv(algorithm, key, iv);
    // Actually decrypt it
    const result = Buffer.concat([decipher.update(encrypted), decipher.final()]);
    return result;
};


module.exports = [
    readFile
]


