
const publicIp = require('public-ip');
let fs = require('fs')

var geoip = require('geoip-lite');
let { downloadModel } = require('../models/downloadfile')
let { encryptFileNodel } = require('../models/encrptfile')


let readFile = (req, res, next) => {
    try {
        encryptFileNodel.findOne({ _id: req.query.fileId }, (err, file) => {
            if (err) {
                return res.status(400).json({ success: false, message: 'something went wrong ', err })
            }
            else if (!file) {
                return res.status(201).json({ success: false, message: 'no such a file', })

            }
            else {
                fs.readFile(`${__dirname}/files/${file.fileName}`, (err, read) => {
                    if (err) {
                        return res.status(400).json({ success: false, message: 'no such file ', err })
                    }
                    else {
                        req.data = {};
                        req.data.file = file;
                        req.data.readFile = read;
                        next()
                    }
                })

            }
        })
    }
    catch (e) {
        return res.status(500).json({ success: false, message: 'internal server error' + e })
    }
}

let downLoadFile = (req, res, next) => {
    try {
        const path = `${__dirname}/files/${req.data.file.fileName}`;
        res.download(path)
        next();
    }
    catch (e) {
        return res.status(500).json({ success: false, message: 'internal server error' + e })
    }
}


let createDownloadHistory = async (req, res) => {
    try {
        let publicip = await publicIp.v4();
        var geo = geoip.lookup(publicip);
        let downloadData = {
            fileName: req.data.file.fileName,
            fileId: req.data.file._id,
            userId: req.decoded.id,
            ip: publicip,
            location: {
                type: "Point",
                coordinates: [Number(geo.ll[1]), Number(geo.ll[0])]
            },
        }

        downloadModel.create(downloadData, (err, created) => {
            if (err) {
                console.log('ERROR' + err)
            }
            else {
                return 1;
            }
        })
    }
    catch (e) {
        return res.status(500).json({ success: false, message: 'internal server error' + e })
    }

}

module.exports = [
    readFile, downLoadFile, createDownloadHistory,
]