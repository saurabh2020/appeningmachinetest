let { userModel } = require('../models/user')
let { hash } = require('../utility/utility')


const createAdmin = async (password) => {
    try {
        let haspwd = await hash(password)
        let admin = await userModel.find({ type: 'admin' });
        if (admin) {
            console.log('already admin create')
        }
        let createAdmin = await userModel.create({
            type: 'admin',
            emailId: 'admin@gmail.com',
            password: haspwd,
        });

        if (createAdmin) {
            console.log('admin create successfully')
        }
    }
    catch (e) {
        console.log('inter server error in create admin')
    }
}


const createuser = async (password) => {
    try {
        let haspwd = await hash(password)
        let randNumber = Math.floor(1000 + Math.random() * 9000);
        let createAdmin = await userModel.create({
            type: 'user',
            emailId: `user${randNumber}@gmail.com`,
            password: haspwd,
        });


        if (createAdmin) {
            console.log('user create successfully')
        }
    }
    catch (e) {
        console.log('inter server error in create user')
    }
}

module.exports = { createAdmin, createuser };


