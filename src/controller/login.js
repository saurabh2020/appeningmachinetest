let { userModel } = require('../models/user');
let { checkHashPassword } = require('../utility/utility')
let jwt = require('jsonwebtoken');
const { config } = require("../config/env");




let checkUser = (req, res, next) => {
    userModel.findOne({ emailId: req.body.emailId }, (err, user) => {
        if (err) {
            return res.status(400).json({ success: false, error: true, message: 'something went wrong', err })
        }
        else if (!user) {
            return res.status(201).json({ success: false, message: 'user not reqiser withthe system.' })
        }
        else {
            req.data = {};
            req.data.user = user;
            next();
        }
    })

}

let matchedPassword = (req, res) => {
    checkHashPassword(req.body.password, req.data.user.password, (err, matched) => {
        if (err) {
            return res.status(400).json({ success: false, message: 'password not matched' })
        }
        else if (matched) {
            let token;
            let payload = {
                id: req.data.user._id,
                emailId: req.data.user.emailId,
                type: req.data.user.type
            }
            if (req.data.user.type == 'user') {
                token = jwt.sign(payload, config.userSecret, {
                    expiresIn: config.tokenValidity.days
                });
            }
            else if (req.data.user.type == 'admin') {
                token = jwt.sign(payload, config.adminSecret, {
                    expiresIn: config.tokenValidity.days
                });
            }
            else {
                return res.status(400).json({ success: false, message: 'invalid user', })
            }

            return res.status(200).json({ success: true, message: 'You are LoggedIn', token: token })

        }
        else {
            return res.status(201).json({ success: false, message: 'password not matched' })
        }
    })

}

module.exports = [
    checkUser,
    matchedPassword
]

